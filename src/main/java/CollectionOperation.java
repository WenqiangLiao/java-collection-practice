import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectionOperation {
    
    public static List<Integer> getListByInterval(int left, int right) {
        // Need to be implemented
        List<Integer> list = new ArrayList<>();
        if (left < right) {
            for (int i = left; i <= right; i++) {
                list.add(i);
            }
        } else {
            for (int i = left; i >= right; i--) {
                list.add(i);
            }
        }
        return list;
    }
    
    
    public static List<Integer> removeLastElement(List<Integer> list) {
        // Need to be implemented
//        list.remove(list.size()-1);
//        return list;
        return list.stream().limit(list.size()-1).collect(Collectors.toList());
    }
    
    public static List<Integer> sortDesc(List<Integer> list) {
        // Need to be implemented
        Collections.sort(list, Collections.reverseOrder());
        return list;
    }
    
    
    public static List<Integer> reverseList(List<Integer> list) {
        // Need to be implemented
        Collections.reverse(list);
        return list;
    }
    
    
    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
//        list1.addAll(list2);
//        return list1;
        return Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
    }
    
    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
//        list2.removeAll(list1);
//        list1.addAll(list2);
//        return list1;
        return Stream.concat(list1.stream(), list2.stream()).distinct().collect(Collectors.toList());
    }
    
    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        if (list1 == null && list2 == null) return true;

        if ((list1 == null && list2 != null) || (list1 != null && list2 == null) || (list1.size() != list2.size())){
            return false;
        }
        Collections.sort(list1);
        Collections.sort(list2);

        return list1.equals(list2);
    }
}
