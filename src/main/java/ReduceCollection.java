import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
        // Need to be implemented
        int max = list.stream().reduce(Integer.MIN_VALUE, (int1, int2) -> int1 > int2?int1:int2);
        return max;
    }
    
    public static double getAverage(List<Integer> list) {
        // Need to be implemented
        double sum = list.stream().reduce(0, (int1, int2) -> int1 + int2);
        return sum/list.size();
    }
    
    public static int getSum(List<Integer> list) {
        // Need to be implemented
        int sum = list.stream().reduce(0,(int1, int2) -> int1 + int2);
        return sum;
    }
    
    public static double getMedian(List<Integer> list) {
        // Need to be implemented
        double median;
        if(list.size() % 2 == 0){
            median = (list.get(list.size() / 2 - 1) + list.get(list.size() / 2)) / 2.0;
        }else{
            median = list.get(list.size()/2);
        }
        return median;
    }
    
    public static int getFirstEven(List<Integer> list) {
        // Need to be implemented

        return list.stream().filter(item -> item%2==0).findFirst().get();
    }
}
