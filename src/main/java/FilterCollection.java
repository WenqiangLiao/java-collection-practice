import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class FilterCollection {
    
    public static List<Integer> getAllEvens(List<Integer> list) {
        // Need to be implemented
        List<Integer> result = new ArrayList<Integer>();
        for(Integer element:list){
            if (element % 2 == 0){
                result.add(element);
            }
        }
        return result;
    }
    
    public static List<Integer> removeDuplicateElements(List<Integer> list) {
        // Need to be implemented
        ArrayList result = new ArrayList<>();
        Iterator it = list.iterator();
        while(it.hasNext()){
            Object obj = it.next();
            if(!result.contains(obj)){
                result.add(obj);
            }
        }
        return result;
    }
    
    public static List<Integer> getCommonElements(List<Integer> collection1, List<Integer> collection2) {
        // Need to be implemented
        List commonElement = new ArrayList<>();
        Iterator it = collection1.iterator();
        while(it.hasNext()){
            Object obj = it.next();
            if(collection2.contains(obj)){
                commonElement.add(obj);
            }
        }
        return commonElement;
    }
}
