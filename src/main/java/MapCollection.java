import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapCollection {
    
    public static List<Integer> doubleCollection(List<Integer> list) {
        // // Need to be implemented
        List<Integer> result = new ArrayList<>();
        for(Integer item:list){
            result.add(item * 2);
        }
        return result;
    }
    
    public static List<String> mapToStringCollection(List<Integer> list) {
        // Need to be implemented
        List<String> result = new ArrayList<>();
        for(Integer item:list){
            char str = (char) (item+96);
            result.add(Character.toString(str));
        }
        return result;
    }
    
    public static List<String> uppercaseCollection(List<String> list) {
        // Need to be implemented
        List<String> result = new ArrayList<>();
        for(String str:list){
            result.add(str.toUpperCase());
        }
        return result;
    }
    
    public static List<Integer> transformTwoDimensionalToOne(List<List<Integer>> list) {
        // Need to be implemented
        List<Integer> result = new ArrayList<>();
        for(List<Integer> array:list) {
            result.addAll(array);
        }
        return result;
    }
}
